package com.example.apripachkin.flickrclient.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.example.apripachkin.flickrclient.R;
import com.example.apripachkin.flickrclient.models.flickr.Photo;
import com.example.apripachkin.flickrclient.models.managers.FullScreenImagePhotoManager;
import com.example.apripachkin.flickrclient.ui.adapters.FullScreenPagerAdapter;

import java.util.List;

public class FullScreenImageActivity extends EventBasedActivity {
    private ViewPager viewPager;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        Intent intent = getIntent();
        position = intent.getIntExtra(POSITION_TO_SHOW, DEFAULT_POSITION);
        FullScreenImagePhotoManager instance = FullScreenImagePhotoManager.getInstance();
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        FullScreenPagerAdapter fullScreenPagerAdapter =
                new FullScreenPagerAdapter(this, instance.getPhotosList());
        viewPager.setAdapter(fullScreenPagerAdapter);
        viewPager.setCurrentItem(position);
        viewPager.setOffscreenPageLimit(3); //cache 3 images before and after current position
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_full_image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
