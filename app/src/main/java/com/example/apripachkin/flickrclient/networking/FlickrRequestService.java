package com.example.apripachkin.flickrclient.networking;

import com.example.apripachkin.flickrclient.models.events.ShowAlertEvent;
import com.example.apripachkin.flickrclient.utils.Constants;
import com.squareup.okhttp.OkHttpClient;

import de.greenrobot.event.EventBus;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;

/**Base class that sets up a retrofit client
 * to make calls to flickr api and return data
 * Created by apripachkin on 8/10/15.
 */
public class FlickrRequestService {

    public FlickrServiceInterface getFlickrService() {
        return getRestAdapter().create(FlickrServiceInterface.class);
    }

    private RestAdapter getRestAdapter() {
        OkHttpClient okHttpClient = new OkHttpClient();
        OkClient okClient = new OkClient(okHttpClient);
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addQueryParam("api_key", Constants.FLICKR_API_KEY);
                request.addQueryParam("format", Constants.FLICKR_RESPONSE_FORMAT);
                request.addQueryParam("nojsoncallback", Constants.FLICKR_NO_JSON_CALLBACK);
                request.addQueryParam("extras", Constants.FLICKR_EXTRAS);
            }
        };
        ErrorHandler errorHandler = new ErrorHandler() {
            @Override
            public Throwable handleError(RetrofitError cause) {
                EventBus eventBus = EventBus.getDefault();
                eventBus.post(new ShowAlertEvent(cause.getMessage(),
                        cause.getLocalizedMessage()));
                return cause;
            }
        };
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.FLICKR_END_POINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(requestInterceptor)
                .setErrorHandler(errorHandler)
                .setClient(okClient)
                .build();
        return restAdapter;
    }
}
