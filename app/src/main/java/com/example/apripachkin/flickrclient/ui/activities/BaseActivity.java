package com.example.apripachkin.flickrclient.ui.activities;

import android.support.v4.app.FragmentActivity;

import de.greenrobot.event.EventBus;

/**
 * Created by apripachkin on 8/11/15.
 */
public abstract class BaseActivity extends FragmentActivity {

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
