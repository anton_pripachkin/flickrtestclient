package com.example.apripachkin.flickrclient.models.managers;

import com.example.apripachkin.flickrclient.models.flickr.FlickrPhotos;

/**
 * Created by apripachkin on 8/11/15.
 */
public class PhotosManager {
    private FlickrPhotos flickrPhotos;

    public PhotosManager(FlickrPhotos flickrPhotos) {
        this.flickrPhotos = flickrPhotos;
    }

    public FlickrPhotos getFlickrPhotos() {
        return flickrPhotos;
    }

    public void setFlickrPhotos(FlickrPhotos flickrPhotos) {
        this.flickrPhotos = flickrPhotos;
    }
}
