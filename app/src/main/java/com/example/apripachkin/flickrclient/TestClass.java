package com.example.apripachkin.flickrclient;

import com.example.apripachkin.flickrclient.models.flickr.FlickrResponse;
import com.example.apripachkin.flickrclient.models.flickr.Photo;
import com.example.apripachkin.flickrclient.models.flickr.FlickrPhotos;
import com.example.apripachkin.flickrclient.networking.FlickrRequestService;
import com.example.apripachkin.flickrclient.networking.FlickrServiceInterface;
import com.example.apripachkin.flickrclient.utils.UrlConstructor;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by apripachkin on 8/11/15.
 */
public class TestClass {
    public static void main(String[] args) {
        FlickrRequestService service = new FlickrRequestService();
        FlickrServiceInterface flickrServiceInterface = service.getFlickrService();
        Callback<FlickrResponse> flickrResultReponseCallback = new Callback<FlickrResponse>() {
            @Override
            public void success(FlickrResponse flickrResultReponse, Response response) {
                FlickrPhotos photos = flickrResultReponse.getPhotos();
                List<Photo> photo = photos.getPhoto();
                for (Photo flickrPhoto : photo) {
                    System.out.println(UrlConstructor.getPhotoUrl(flickrPhoto));
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        };
        flickrServiceInterface.searchPhotosByKeyword(1, "Hello!", flickrResultReponseCallback);
    }
}
