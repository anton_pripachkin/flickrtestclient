package com.example.apripachkin.flickrclient.ui.fragments;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;

import com.example.apripachkin.flickrclient.R;
import com.example.apripachkin.flickrclient.models.events.ShowAlertEvent;
import com.example.apripachkin.flickrclient.ui.activities.LocationBasedActivity;
import com.example.apripachkin.flickrclient.ui.activities.MainActivity;
import com.example.apripachkin.flickrclient.utils.Constants;

import de.greenrobot.event.EventBus;

/**
 * Fragment provides ability to show nearby photos using current user location
 */
public class SearchByLocationFragment extends BaseFragment {
    private Button btnGetByLocation;
    private LocationBasedActivity locationBasedActivity;
    private Location location;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        locationBasedActivity = (LocationBasedActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_by_location, container, false);
        btnGetByLocation = (Button) view.findViewById(R.id.btnGetByLocation);
        gridView = (GridView) view.findViewById(R.id.grid_location);
        btnLoadMore = (Button) view.findViewById(R.id.btnLoadMore);
        btnLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadNextPage();
            }
        });
        btnGetByLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                location = locationBasedActivity.getLocation();
                if (location == null) {
//                    EventBus.getDefault().post(new ShowAlertEvent("Please enable GPS and try again"," Unable to detect location"));
                    locationBasedActivity.requestUserEnableGps();
                    return;
                }
                Log.d("SearchByLocation", "Location " + location);
                flickrManager.getPhotosByLocation(location, Constants.DEFAULT_RADIUS, Constants.DEFAULT_PAGE);
            }
        });
        return view;
    }


    @Override
    protected void loadNextPage() {
        flickrManager.getPhotosByLocation(location, Constants.DEFAULT_RADIUS, ++page);
    }
}
