package com.example.apripachkin.flickrclient.models.events;

import com.example.apripachkin.flickrclient.models.managers.PhotosManager;

/**
 * Created by apripachkin on 8/11/15.
 */
public class PhotosDownloadedEvent {
    private PhotosManager photosManager;

    public PhotosDownloadedEvent(PhotosManager photosManager) {
        this.photosManager = photosManager;
    }

    public PhotosManager getPhotosManager() {
        return photosManager;
    }

    public void setPhotosManager(PhotosManager photosManager) {
        this.photosManager = photosManager;
    }
}
