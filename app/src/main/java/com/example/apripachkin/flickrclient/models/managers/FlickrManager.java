package com.example.apripachkin.flickrclient.models.managers;

import android.location.Location;

import com.example.apripachkin.flickrclient.models.events.HideDialogEvent;
import com.example.apripachkin.flickrclient.models.events.PhotosDownloadedEvent;
import com.example.apripachkin.flickrclient.models.events.ShowAlertEvent;
import com.example.apripachkin.flickrclient.models.events.ShowDialogEvent;
import com.example.apripachkin.flickrclient.models.flickr.FlickrResponse;
import com.example.apripachkin.flickrclient.networking.FlickrRequestService;
import com.example.apripachkin.flickrclient.networking.FlickrServiceInterface;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**Class that provides communication with flickr api
 * sends call back to activity using EventBuss events
 * Created by apripachkin on 8/11/15.
 */
public class FlickrManager {
    private FlickrServiceInterface flickrServiceInterface;
    private Callback<FlickrResponse> callback;


    public FlickrManager(FlickrRequestService service) {
        flickrServiceInterface = service.getFlickrService();
        callback = new Callback<FlickrResponse>() {
            @Override
            public void success(FlickrResponse flickrResponse, Response response) {
                EventBus.getDefault().post(new HideDialogEvent());
                PhotosManager photosManager = new PhotosManager(flickrResponse.getPhotos());
                EventBus.getDefault().post(new PhotosDownloadedEvent(photosManager));
            }

            @Override
            public void failure(RetrofitError error) {
                EventBus eventBus = EventBus.getDefault();
                eventBus.post(new HideDialogEvent());
                eventBus.post(new ShowAlertEvent(error.getMessage(),
                                                 error.getLocalizedMessage()));
            }
        };

    }

    public void getPhotosByText(String text, int page) {
        EventBus.getDefault().post(new ShowDialogEvent("Loading photos by text"));
        flickrServiceInterface.searchPhotosByKeyword(page, text, callback);
    }

    public void getPhotosByLocation(Location location, int radius, int page) {
        EventBus.getDefault().post(new ShowDialogEvent("Loading photos within " + radius + "km"));
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        flickrServiceInterface.searchByNearByLocation(latitude, longitude, radius, page, callback);
    }
}
