package com.example.apripachkin.flickrclient.models.flickr;

/**
 * Created by apripachkin on 8/10/15.
 */
public class FlickrResponse {
    private FlickrPhotos photos;

    public FlickrPhotos getPhotos() {
        return photos;
    }

    public void setPhotos(FlickrPhotos photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        return "FlickrResponse{" +
                "photos=" + photos +
                '}';
    }
}
