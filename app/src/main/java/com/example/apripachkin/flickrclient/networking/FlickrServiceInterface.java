package com.example.apripachkin.flickrclient.networking;

import com.example.apripachkin.flickrclient.models.flickr.FlickrResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Contains all rest api calls
 * to flickr for use in retrofit api
 * Created by apripachkin on 8/4/15.
 */
public interface FlickrServiceInterface {
    @GET("/?method=flickr.photos.search")
    void searchPhotosByKeyword(@Query("page") int page,
                               @Query("text") String text,
                               Callback<FlickrResponse> callback);


    @GET("/?method=flickr.photos.search")
    void searchByNearByLocation(@Query("lat") double latitude,
                                @Query("lon") double longitude,
                                @Query("radius") int radius,
                                @Query("page") int page,
                                Callback<FlickrResponse> callback);
}
