package com.example.apripachkin.flickrclient.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;

import com.example.apripachkin.flickrclient.R;
import com.example.apripachkin.flickrclient.models.events.ShowAlertEvent;
import com.example.apripachkin.flickrclient.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import de.greenrobot.event.EventBus;

import static com.google.android.gms.common.api.GoogleApiClient.*;

/**
 * Created by apripachkin on 8/14/15.
 */
public class LocationBasedActivity extends BaseActivity implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    protected GoogleApiClient googleApiClient;
    protected Location location;
    private boolean resolvingError = false;
    private boolean requestingLocationUpdates = false;
    private static final int REQUEST_RESOLVE_ERROR = 10001;


    @Override
    protected void onStart() {
        super.onStart();
        if (!resolvingError) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient.isConnecting() && !requestingLocationUpdates) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildGoogleApiClient();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            resolvingError = false;
            if (resultCode == RESULT_OK) {
                if (!googleApiClient.isConnecting() &&
                        !googleApiClient.isConnected()) {
                    googleApiClient.connect();
                }
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location == null) {
            startLocationUpdates();
        }
    }

    protected void startLocationUpdates() {
        LocationRequest locationRequest = createLocationRequest();
        requestingLocationUpdates = true;
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        //not implemented
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (resolvingError) {
            return;
        } else if (connectionResult.hasResolution()) {
            try {
                resolvingError = true;
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException ex) {
                googleApiClient.connect();
            }
        } else {
            String title = "Error: " + connectionResult.getErrorCode();
            String message = connectionResult.toString();
            EventBus.getDefault().post(new ShowAlertEvent(title, message));
            resolvingError = true;
        }
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        googleApiClient.connect();
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(Constants.NORMAL_INTERVAL);
        locationRequest.setFastestInterval(Constants.FASTEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    public void onDialogDismissed() {
        resolvingError = false;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    protected void stopLocationUpdates() {
        requestingLocationUpdates = false;
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    public Location getLocation () {
        return location;
    }


    public void requestUserEnableGps() {
        DialogInterface.OnClickListener negativeButtonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        DialogInterface.OnClickListener positiveButtonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        };
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.gpsNotEnabledTitle)
                .setMessage(R.string.enableGpsMessage)
                .setPositiveButton(R.string.openSettings, negativeButtonListener)
                .setNegativeButton(R.string.exitApp, positiveButtonListener).show();
    }

}