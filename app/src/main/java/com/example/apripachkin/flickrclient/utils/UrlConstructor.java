package com.example.apripachkin.flickrclient.utils;

import com.example.apripachkin.flickrclient.models.flickr.Photo;

/**Constructs direct URL for image to be shown
 * using Photo object instance
 * Created by apripachkin on 8/11/15.
 */
public class UrlConstructor {

    private UrlConstructor() {}

    public static String getPhotoUrl(Photo photo) {
        int farm = photo.getFarm();
        int server = photo.getServer();
        long id = photo.getId();
        String secret = photo.getSecret();
        String url = "https://farm" + farm + ".staticflickr.com/" + server
                + "/" + id + "_" + secret + ".jpg";
        return url;
    }
}
