package com.example.apripachkin.flickrclient.ui.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.apripachkin.flickrclient.R;
import com.example.apripachkin.flickrclient.models.events.HideDialogEvent;
import com.example.apripachkin.flickrclient.models.events.ShowAlertEvent;
import com.example.apripachkin.flickrclient.models.events.ShowDialogEvent;
import com.example.apripachkin.flickrclient.models.events.ShowFullScreenEvent;
import com.example.apripachkin.flickrclient.models.managers.FullScreenImagePhotoManager;
import com.example.apripachkin.flickrclient.utils.Constants;
import com.google.android.gms.common.ConnectionResult;


/**
 * Created by apripachkin on 8/13/15.
 */
public class EventBasedActivity extends LocationBasedActivity {
    private ProgressDialog progressDialog;
    public void onEvent(ShowDialogEvent showDialogEvent) {
        progressDialog = ProgressDialog.show(this, getResources().getString(R.string.loading),
                showDialogEvent.getMessage());
    }

    public void onEvent(HideDialogEvent hideDialogEvent) {
        if (progressDialog == null) {
            return;
        }
        progressDialog.dismiss();
        progressDialog = null;
    }

    public void onEvent(ShowAlertEvent showAlertEvent) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(showAlertEvent.getTitle())
                .setMessage(showAlertEvent.getMessage())
                .setPositiveButton(android.R.string.ok, null)
                .show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                onDialogDismissed();
            }
        });
    }

    public void onEvent(ShowFullScreenEvent showFullScreenEvent) {
        FullScreenImagePhotoManager instance = FullScreenImagePhotoManager.getInstance();
        instance.setPhotosList(showFullScreenEvent.getPhotosList());
        int position = showFullScreenEvent.getPosition();
        Intent intent = new Intent(this, FullScreenImageActivity.class);
        intent.putExtra(Constants.POSITION_TO_SHOW, position);
        startActivity(intent);
    }


}
