package com.example.apripachkin.flickrclient.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.apripachkin.flickrclient.R;
import com.example.apripachkin.flickrclient.models.flickr.Photo;
import com.example.apripachkin.flickrclient.utils.UrlConstructor;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by apripachkin on 8/11/15.
 */
public class FullScreenPagerAdapter extends PagerAdapter {
    private Context context;
    private ImageView imageView;
    private List<Photo> photos;

    public FullScreenPagerAdapter(Context context, List<Photo> photos) {
        this.context = context;
        this.photos = photos;
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater layoutInflater = ((Activity) context).getLayoutInflater();
        View inflatedView = layoutInflater.inflate(R.layout.full_screen_image, container, false);
        imageView = (ImageView) inflatedView.findViewById(R.id.imgFullScreen);
        Photo photo = photos.get(position);
        String url = null;
        String urlOriginalSize = photo.getUrl_l();
        if (urlOriginalSize == null) {
            url = UrlConstructor.getPhotoUrl(photo);
        } else {
            url = urlOriginalSize;
        }
        Picasso.with(context).load(url).placeholder(R.drawable.placeholder).into(imageView);
        ((ViewPager) container).addView(inflatedView, 0);
        return inflatedView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView(((LinearLayout) object));
    }

}
