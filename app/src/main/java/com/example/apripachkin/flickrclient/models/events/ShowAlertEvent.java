package com.example.apripachkin.flickrclient.models.events;

/**
 * Created by apripachkin on 8/12/15.
 */
public class ShowAlertEvent {
    private String message;
    private String title;

    public ShowAlertEvent(String message, String title) {
        this.message = message;
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }
}
