package com.example.apripachkin.flickrclient.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;

import com.example.apripachkin.flickrclient.R;
import com.example.apripachkin.flickrclient.utils.Constants;

/**
 * Fragment that provides ability to search images via text search
 */
public class SearchByTextFragment extends BaseFragment {
    private Button btnSubmitText;
    private EditText editText;
    private String searchQuerty;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_by_text, container, false);
        initializeViews(view);
        return view;
    }

    private void initializeViews(View view) {
        btnSubmitText = (Button) view.findViewById(R.id.btnSendRequestText);
        btnLoadMore = (Button) view.findViewById(R.id.btnLoadMore);
        editText = (EditText) view.findViewById(R.id.et_text_to_search);
        gridView = (GridView) view.findViewById(R.id.grid_text_images);

        btnLoadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNextPage();
            }
        });
        btnSubmitText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(editText)) {
                    return;
                }
                searchQuerty = editText.getText().toString();
                flickrManager.getPhotosByText(searchQuerty, Constants.DEFAULT_PAGE);
            }
        });
    }

    @Override
    protected void loadNextPage() {
        flickrManager.getPhotosByText(searchQuerty, ++page);
    }


    private boolean isEmpty(EditText editText) {
        return editText.getText().toString().trim().length() == 0;
    }

}
