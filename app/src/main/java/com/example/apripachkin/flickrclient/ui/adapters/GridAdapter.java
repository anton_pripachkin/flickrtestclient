package com.example.apripachkin.flickrclient.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.apripachkin.flickrclient.R;
import com.example.apripachkin.flickrclient.models.flickr.Photo;
import com.example.apripachkin.flickrclient.utils.UrlConstructor;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by apripachkin on 8/11/15.
 */
public class GridAdapter extends BaseAdapter {
    private List<Photo> photos;
    private Context context;

    public GridAdapter(List<Photo> photos, Context context) {
        this.photos = photos;
        this.context = context;
    }

    public void addMoreElements(List<Photo> newPhotos) {
        photos.addAll(newPhotos);
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Object getItem(int position) {
        return photos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = ((Activity) context).getLayoutInflater();
            convertView = layoutInflater.inflate(R.layout.single_image_grid, parent, false);
        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.grid_image);
        Photo photo = (Photo) getItem(position);
        String photoUrl = UrlConstructor.getPhotoUrl(photo);
        Picasso.with(context).load(photoUrl).
                              placeholder(R.drawable.placeholder).
                              into(imageView);
        return convertView;
    }

}
