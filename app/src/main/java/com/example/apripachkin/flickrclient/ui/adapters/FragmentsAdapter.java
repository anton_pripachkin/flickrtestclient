package com.example.apripachkin.flickrclient.ui.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.apripachkin.flickrclient.ui.fragments.SearchByLocationFragment;
import com.example.apripachkin.flickrclient.ui.fragments.SearchByTextFragment;
import com.example.apripachkin.flickrclient.utils.Constants;

/**Provides fragments to be shown in MainActivity ViewPager
 * Created by apripachkin on 8/13/15.
 */
public class FragmentsAdapter extends FragmentPagerAdapter{
    private final int PAGE_COUNT = 2;
    private String [] tabTitles = {Constants.TAB_BY_TEXT, Constants.TAB_BY_LOCATION};
    private Context context;
    public FragmentsAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case Constants.SEARCH_BY_TEXT:
                fragment = new SearchByTextFragment();
                break;
            case Constants.SEARCH_BY_LOCATION:
                fragment = new SearchByLocationFragment();
                break;
        }
        return fragment;
    }



    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
