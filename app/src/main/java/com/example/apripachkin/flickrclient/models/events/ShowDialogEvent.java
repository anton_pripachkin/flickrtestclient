package com.example.apripachkin.flickrclient.models.events;

/**
 * Created by apripachkin on 8/11/15.
 */
public class ShowDialogEvent {
    private String message;

    public ShowDialogEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
