package com.example.apripachkin.flickrclient.utils;

/**
 * Created by apripachkin on 8/4/15.
 */
public class Constants {
    private Constants () {}
    public static final String FLICKR_END_POINT = "https://api.flickr.com/services/rest";

    public static final String FLICKR_API_KEY = "194c6feb1264f63e68bcb6303cffc2be";
    public static final String FLICKR_SECRET = "9e8bc099f3d0bd7f";

    public static final String FLICKR_RESPONSE_FORMAT = "json";
    public static final String FLICKR_NO_JSON_CALLBACK = "1";
    public static final String FLICKR_EXTRAS = "url_l,url_o";
    public static final String FULL_SCREEN_PHOTOS = "fullScreenPhotos";
    public static final String POSITION_TO_SHOW = "position_to_show";

    public static final int DEFAULT_PAGE = 1;
    public static final int SEARCH_BY_TEXT = 0;
    public static final int SEARCH_BY_LOCATION = 1;

    public static final String TAB_BY_TEXT = "Find photos by text";
    public static final String TAB_BY_LOCATION = "Find photos by location";

    public static final int DEFAULT_POSITION = 0;
    public static final int DEFAULT_RADIUS = 1;
    public static final int NORMAL_INTERVAL = 10000;
    public static final int FASTEST_INTERVAL = 5000;
}
