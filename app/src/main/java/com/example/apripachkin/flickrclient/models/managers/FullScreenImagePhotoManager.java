package com.example.apripachkin.flickrclient.models.managers;

import com.example.apripachkin.flickrclient.models.flickr.Photo;

import java.util.List;

/**
 * Created by apripachkin on 8/14/15.
 */
public class FullScreenImagePhotoManager {
    private static FullScreenImagePhotoManager instance;
    private List<Photo> photosList;

    private FullScreenImagePhotoManager () {}

    public static FullScreenImagePhotoManager getInstance() {
        if (instance == null) {
            instance = new FullScreenImagePhotoManager();
        }
        return instance;
    }

    public List<Photo> getPhotosList() {
        return photosList;
    }

    public void setPhotosList(List<Photo> photosList) {
        this.photosList = photosList;
    }

    public void clearPhotosList() {
        photosList.clear();
    }
}
