package com.example.apripachkin.flickrclient.models.flickr;

/**
 * Created by apripachkin on 8/10/15.
 */
public class Photo  {
    private long id;
    private String owner;
    private String secret;
    private int server;
    private int farm;
    private String title;
    private String url_o;
    private String url_l;
    private int isPublic;
    private int isFriend;
    private int isFamily;

    public String getUrl_l() {
        return url_l;
    }

    public void setUrl_l(String url_l) {
        this.url_l = url_l;
    }

    public String getUrl_o() {
        return url_o;
    }

    public void setUrl_o(String url_o) {
        this.url_o = url_o;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public int getServer() {
        return server;
    }

    public void setServer(int server) {
        this.server = server;
    }

    public int getFarm() {
        return farm;
    }

    public void setFarm(int farm) {
        this.farm = farm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public int getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(int isFriend) {
        this.isFriend = isFriend;
    }

    public int getIsFamily() {
        return isFamily;
    }

    public void setIsFamily(int isFamily) {
        this.isFamily = isFamily;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", owner='" + owner + '\'' +
                ", secret='" + secret + '\'' +
                ", server=" + server +
                ", farm=" + farm +
                ", title='" + title + '\'' +
                ", url_o='" + url_o + '\'' +
                ", url_l='" + url_l + '\'' +
                ", isPublic=" + isPublic +
                ", isFriend=" + isFriend +
                ", isFamily=" + isFamily +
                '}';
    }
}
