package com.example.apripachkin.flickrclient.ui.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.example.apripachkin.flickrclient.R;
import com.example.apripachkin.flickrclient.models.events.PhotosDownloadedEvent;
import com.example.apripachkin.flickrclient.models.flickr.FlickrPhotos;
import com.example.apripachkin.flickrclient.models.managers.PhotosManager;
import com.example.apripachkin.flickrclient.ui.adapters.FragmentsAdapter;
import com.example.apripachkin.flickrclient.ui.fragments.BaseFragment;
import com.example.apripachkin.flickrclient.utils.Constants;


public class MainActivity extends EventBasedActivity {
    private FragmentManager fragmentManager;
    private ViewPager viewPagerMain;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPagerMain = (ViewPager) findViewById(R.id.viewPagerMain);
        fragmentManager = getSupportFragmentManager();
        viewPagerMain.setAdapter(new FragmentsAdapter(fragmentManager, this));
        tabLayout = (TabLayout) findViewById(R.id.fixed_tabs);
        tabLayout.setupWithViewPager(viewPagerMain);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onEvent(PhotosDownloadedEvent photosDownloadedEvent) {
        PhotosManager photosManager = photosDownloadedEvent.getPhotosManager();
        FlickrPhotos flickrPhotos = photosManager.getFlickrPhotos();
        int currentItem = viewPagerMain.getCurrentItem();
        showPhotos(flickrPhotos, currentItem);
    }

    private void showPhotos(FlickrPhotos flickrPhotos, int currentItem) {
        BaseFragment baseFragment = (BaseFragment) getFragmentByPosition(currentItem);
        if (flickrPhotos.getPage() > Constants.DEFAULT_PAGE) {
            baseFragment.showMorePhotos(flickrPhotos.getPhoto());
        } else {
            baseFragment.showPhotos(flickrPhotos);
        }
    }

    /**
     * Retrieving reference to existing Fragment created by FragmentPagerAdapter
     * See: <a href="http://stackoverflow.com/questions/8785221/retrieve-a-fragment-from-a-viewpager">
     *     StackOverflow </a> for full description
     * @param position Fragment at current ViewPager position
     * @return existing fragment found by tag
     */
    private Fragment getFragmentByPosition(int position) {
        String tag = "android:switcher:" + viewPagerMain.getId() + ":" + position;
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

}
