package com.example.apripachkin.flickrclient.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.example.apripachkin.flickrclient.models.events.ShowFullScreenEvent;
import com.example.apripachkin.flickrclient.models.flickr.FlickrPhotos;
import com.example.apripachkin.flickrclient.models.flickr.Photo;
import com.example.apripachkin.flickrclient.models.managers.FlickrManager;
import com.example.apripachkin.flickrclient.networking.FlickrRequestService;
import com.example.apripachkin.flickrclient.ui.adapters.GridAdapter;
import com.example.apripachkin.flickrclient.utils.Constants;

import java.util.List;

import de.greenrobot.event.EventBus;

/** Base fragment provides functionality to show images
 *in GridView after receiving a response from external resource as
 * well function to load more photos (if available) and update GridView adapter
 * Inherited classes must override {@link #loadNextPage() loadNextPage} method
 * with proper request
 * Created by apripachkin on 8/13/15.
 */
public abstract class BaseFragment extends Fragment implements AbsListView.OnScrollListener {
    protected int numOfPages;
    protected int page;
    protected GridView gridView;
    protected GridAdapter gridAdapter;
    protected Button btnLoadMore;
    protected FlickrManager flickrManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        flickrManager = new FlickrManager(new FlickrRequestService());
    }

    public void showPhotos(FlickrPhotos flickrPhotos) {
        btnLoadMore.setVisibility(View.GONE);

        final List<Photo> photo = flickrPhotos.getPhoto();
        numOfPages = flickrPhotos.getPages();
        page = flickrPhotos.getPage();

        gridAdapter = new GridAdapter(photo, getActivity());
        gridView.setAdapter(gridAdapter);
        gridView.setOnScrollListener(this);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventBus.getDefault().post(new ShowFullScreenEvent(photo, position));
            }
        });
    }

    public void showMorePhotos(List<Photo> newPhotos) {
        gridAdapter = (GridAdapter) gridView.getAdapter();
        int startOfNewList = gridAdapter.getCount();

        btnLoadMore.setVisibility(View.GONE);

        gridAdapter.addMoreElements(newPhotos);
        gridAdapter.notifyDataSetChanged();

        gridView.smoothScrollToPosition(startOfNewList);
    }

    protected abstract void loadNextPage();

    protected boolean canLoadNextPage() {
        return page < numOfPages;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        //do nothing
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int lastVisiblePosition = gridView.getLastVisiblePosition();

        GridAdapter gridAdapter = (GridAdapter) gridView.getAdapter();

        int totalElements = gridAdapter.getCount();
        int closeToEnd = lastVisiblePosition + 1;

        if (closeToEnd == totalElements) {
            if (canLoadNextPage()) {
                btnLoadMore.setVisibility(View.VISIBLE);
            }
        }
    }
}
