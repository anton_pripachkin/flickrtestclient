package com.example.apripachkin.flickrclient.models.events;

import com.example.apripachkin.flickrclient.models.flickr.Photo;

import java.util.List;

/**
 * Created by apripachkin on 8/11/15.
 */
public class ShowFullScreenEvent {
    private List<Photo> photosList;
    private int position;

    public ShowFullScreenEvent(List<Photo> photosList, int position) {
        this.photosList = photosList;
        this.position = position;
    }

    public List<Photo> getPhotosList() {
        return photosList;
    }

    public int getPosition() {
        return position;
    }
}
